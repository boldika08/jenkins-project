package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.*;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {

    public static Service service;

    @BeforeAll
    public static void setup() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);

        System.out.println("BeforeAll done!");
    }

    @BeforeEach
    public void clearingTaskBefore() {
        for (Student student : service.findAllStudents()) {
            service.deleteStudent(student.getID());
        }
        for (Homework homework : service.findAllHomework()) {
            service.deleteHomework(homework.getID());
        }
        System.out.println("Cleaning done with BeforeEach!");
    }

    @AfterEach
    public void clearingTaskAfter() {
        for (Student student : service.findAllStudents()) {
            service.deleteStudent(student.getID());
        }
        for (Homework homework : service.findAllHomework()) {
            service.deleteHomework(homework.getID());
        }
        System.out.println("Cleaning done with AfterEach!");
    }

    @org.junit.jupiter.api.Test
    void findAllStudents() {
        Student student1 = new Student("1", "Carl Johnson", 533);
        Student student2 = new Student("2", "Big Smoke", 533);
        Student student3 = new Student("3", "Ryder Mate", 533);

        service.saveStudent(student1.getID(), student1.getName(), student1.getGroup());
        service.saveStudent(student2.getID(), student2.getName(), student2.getGroup());
        service.saveStudent(student3.getID(), student3.getName(), student3.getGroup());

        int counter = 0;
        for (Student student : service.findAllStudents()) {
            counter++;
        }

        assertEquals(3, counter);

        service.deleteStudent("1");
        service.deleteStudent("2");
        service.deleteStudent("3");
    }

    @org.junit.jupiter.api.Test
    void findAllHomework() {
        Homework homework1 = new Homework("1", "Lorem Ipsum1", 8, 2);
        Homework homework2 = new Homework("2", "Lorem Ipsum2", 6, 1);
        int result1 = service.saveHomework(homework1.getID(), homework1.getDescription(), homework1.getDeadline(),
                homework1.getStartline());
        int result2 = service.saveHomework(homework2.getID(), homework2.getDescription(), homework2.getDeadline(),
                homework2.getStartline());

        int counter = 0;
        for (Homework homework : service.findAllHomework()) {
            counter++;
        }
        int finalCounter = counter;

        assertAll(
                () -> {assertEquals(0, result1);},
                () -> {assertEquals(0, result2);},
                () -> {assertEquals(2, finalCounter);}
        );

        service.deleteHomework("1");
        service.deleteHomework("2");
    }

    @org.junit.jupiter.api.Test
    void findAllGrades() {
        int counter = 0;
        for (Grade grade : service.findAllGrades()) {
            counter++;
        }

        boolean passed=false;

        if (counter == 2)
            passed=true;

        assertTrue(passed);

    }

    @org.junit.jupiter.api.Test
    void saveValidStudent() {
        Student student1 = new Student("1", "Carl Johnson", 533);
        int result = service.saveStudent(student1.getID(), student1.getName(), student1.getGroup());
        assertEquals(0, result);
        service.deleteStudent("1");
    }

    @org.junit.jupiter.api.Test
    void saveValidHomework() {
        Homework homework = new Homework("15", "Lorem Ipsum", 6, 2);
        int result = service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                homework.getStartline());
        assertNotEquals(1, result);
        service.deleteHomework("15");
    }

    @org.junit.jupiter.api.Test
    void saveInvalidHomework() {
        Homework homework = new Homework("15", "Lipsum", 1, 2);
        assertThrows(ValidationException.class, () ->
                service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                        homework.getStartline()));
        service.deleteHomework("15");
    }

    @org.junit.jupiter.api.Test
    void saveInvalidGrade() {
        int result = service.saveGrade("1", "1", 10, 7, "done");
        assertEquals(-1, result);
    }

    @org.junit.jupiter.api.Test
    void deleteStudent() {
        Student student1 = new Student("1", "Carl Johnson", 533);
        Student student2 = new Student("2", "Big Smoke", 533);
        Student student3 = new Student("3", "Ryder Mate", 533);

        service.saveStudent(student1.getID(), student1.getName(), student1.getGroup());
        service.saveStudent(student2.getID(), student2.getName(), student2.getGroup());
        service.saveStudent(student3.getID(), student3.getName(), student3.getGroup());

        service.deleteStudent("3");

        int counter = 0;
        for (Student student : service.findAllStudents()) {
            counter++;
        }

        assertEquals(2, counter);

        service.deleteStudent("1");
        service.deleteStudent("2");

    }

    @DisplayName("Delete HW")
    @org.junit.jupiter.api.Test
    void deleteHomework() {
        Homework homework1 = new Homework("1", "Lorem Ipsum1", 8, 2);
        Homework homework2 = new Homework("2", "Lorem Ipsum2", 6, 1);
        int result1 = service.saveHomework(homework1.getID(), homework1.getDescription(), homework1.getDeadline(),
                homework1.getStartline());
        int result2 = service.saveHomework(homework2.getID(), homework2.getDescription(), homework2.getDeadline(),
                homework2.getStartline());

        service.deleteHomework("2");

        int counter = 0;
        for (Homework homework : service.findAllHomework()) {
            counter++;
        }
        int finalCounter = counter;

        assertAll(
                () -> {assertEquals(0, result1);},
                () -> {assertEquals(0, result2);},
                () -> {assertEquals(1, finalCounter);}
        );

        service.deleteHomework("1");

    }

    @org.junit.jupiter.api.Test
    void updateStudent() {
    }

    @org.junit.jupiter.api.Test
    void updateValidHomework() {
        Homework homework1 = new Homework("1", "Lorem Ipsum1", 8, 2);
        service.saveHomework(homework1.getID(), homework1.getDescription(), homework1.getDeadline(),
                homework1.getStartline());

        int result1 = service.updateHomework("1", "Not Loremipsum", 20, 1);

        assertEquals(0, result1);

        service.deleteHomework("1");
    }

    @org.junit.jupiter.api.Test
    void updateInvalidHomework() {
        Homework homework1 = new Homework("1", "Lorem Ipsum1", 8, 2);
        service.saveHomework(homework1.getID(), homework1.getDescription(), homework1.getDeadline(),
                homework1.getStartline());

        assertThrows(ValidationException.class, () ->
                service.updateHomework("1", "Not Loremipsum", 20, 1));

        service.deleteHomework("1");
    }

    @org.junit.jupiter.api.Test
    void extendDeadline() {
    }

    @org.junit.jupiter.api.Test
    void createStudentFile() {
    }


    @ParameterizedTest
    @ValueSource(strings = ("1"))
    void createStudents(String id) {
        Student student1 = new Student(id, "Carl Johnson", 533);
        int result = service.saveStudent(student1.getID(), student1.getName(), student1.getGroup());
        assertEquals(0, result);
        service.deleteStudent("1");
    }

    @AfterAll
    public static void jobDone() {
        System.out.println("Job's done!");
    }

}